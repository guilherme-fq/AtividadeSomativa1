FROM ubuntu:latest

RUN apt-get update
RUN apt-get install nginx -y
RUN mkdir /exemplo_html
WORKDIR /exemplo_html
COPY cadastro.html /var/www/html

EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
